"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var calibration_1 = require("./calibration");
var cali = calibration_1.CALIBRATION_DOC;
var calibrationLines = calibration_1.CALIBRATION_DOC.split("\n");
var findNumbers = function (line) {
    var matches = line.match(/\d+/g);
    var numbers = [];
    if (matches.length) {
        numbers = matches.map(function (item) { return parseInt(item); });
    }
    return numbers;
};
var getFirstDigit = function (fullNumber) {
    return parseInt(fullNumber.toString().charAt(0));
};
var getLastDigit = function (fullNumber) {
    return parseInt(fullNumber.toString().charAt(fullNumber.toString().length - 1));
};
var getLineNumber = function (first, second) {
    return parseInt(first.toString() + second.toString());
};
var counter = 0;
for (var _i = 0, calibrationLines_1 = calibrationLines; _i < calibrationLines_1.length; _i++) {
    var calibrationLine = calibrationLines_1[_i];
    var numbers = findNumbers(calibrationLine);
    var firstDigit = getFirstDigit(numbers[0]);
    var lastDigit = getLastDigit(numbers[numbers.length - 1]);
    var lineNumber = getLineNumber(firstDigit, lastDigit);
    // console.log(calibrationLine, ": " , lineNumber);
    console.log(calibrationLine, ": ", lineNumber, " - ", firstDigit, " - ", lastDigit, " | ", numbers);
    counter += lineNumber;
}
console.log("count: ", counter);
