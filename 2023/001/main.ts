import {CALIBRATION_DOC} from "./calibration";

const cali = CALIBRATION_DOC;

const calibrationLines = CALIBRATION_DOC.split("\n");

const findNumbers = (line: string) => {
  const matches = line.match(/\d+/g);

  let numbers: number[] = [];

  if (matches.length) {
      numbers = matches.map(item => parseInt(item));
  }

  return numbers;
};

const getFirstDigit = (fullNumber: number): number => {
    return parseInt(fullNumber.toString().charAt(0));
};

const getLastDigit = (fullNumber: number): number => {
    return parseInt(fullNumber.toString().charAt(fullNumber.toString().length - 1));
};

const getLineNumber = (first: number, second: number): number => {
    return parseInt(first.toString() + second.toString());
};

let counter = 0;

for (const calibrationLine of calibrationLines) {
    const numbers = findNumbers(calibrationLine);
    const firstDigit = getFirstDigit(numbers[0]);
    const lastDigit = getLastDigit(numbers[numbers.length - 1]);

    const lineNumber = getLineNumber(firstDigit, lastDigit);

    // console.log(calibrationLine, ": " , lineNumber);

    console.log(calibrationLine, ": ", lineNumber, " - ", firstDigit, " - ", lastDigit, " | ",  numbers);
    counter += lineNumber;
}

console.log("count: ", counter);
