"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const calibration_1 = require("./calibration");
const TEXT_NUMBERS = "one, two, three, four, five, six, seven, eight, nine".split(", ");
const calibrationLines = calibration_1.CALIBRATION_DOC.split("\n");
const findNumbers = (line) => {
    let numbers = [];
    let str = line;
    while (str.length) {
        let number = checkNumber(str);
        if (number) {
            numbers.push(number);
        }
        str = str.slice(1);
    }
    return numbers;
};
const stringToNumber = (testString, string) => {
    if (testString.startsWith(string)) {
        return TEXT_NUMBERS.indexOf(string) + 1;
    }
    else {
        return null;
    }
};
const checkNumber = (testString) => {
    var _a;
    let foundNumber = null;
    for (const string of TEXT_NUMBERS) {
        const check = stringToNumber(testString, string);
        if (check !== null) {
            foundNumber = check;
            break;
        }
    }
    if (foundNumber === null) {
        foundNumber = (_a = parseInt(testString)) !== null && _a !== void 0 ? _a : null;
    }
    return foundNumber;
};
const getFirstDigit = (fullNumber) => {
    return parseInt(fullNumber.toString().charAt(0));
};
const getLastDigit = (fullNumber) => {
    return parseInt(fullNumber.toString().charAt(fullNumber.toString().length - 1));
};
const getLineNumber = (first, second) => {
    return parseInt(first.toString() + second.toString());
};
let counter = 0;
for (const calibrationLine of calibrationLines) {
    const numbers = findNumbers(calibrationLine);
    const firstDigit = getFirstDigit(numbers[0]);
    const lastDigit = getLastDigit(numbers[numbers.length - 1]);
    const lineNumber = getLineNumber(firstDigit, lastDigit);
    console.log(calibrationLine, ": ", lineNumber, " - ", firstDigit, " - ", lastDigit, " | ", numbers);
    counter += lineNumber;
}
console.log("count: ", counter);
