import {CALIBRATION_DOC} from "./calibration";

const TEXT_NUMBERS = "one, two, three, four, five, six, seven, eight, nine".split(", ");
const calibrationLines = CALIBRATION_DOC.split("\n");

const findNumbers = (line: string) => {

  let numbers: number[] = [];

  let str = line;
  while(str.length) {
      let number = checkNumber(str);

      if(number) {
            numbers.push(number);
      }

      str = str.slice(1);
  }

  return numbers;
};

const stringToNumber = (testString: string, string: string): number | null => {
    if(testString.startsWith(string)) {
        return TEXT_NUMBERS.indexOf(string) + 1;
    } else {
        return null;
    }
};

const checkNumber = (testString: string): number | null => {

    let foundNumber = null;


    for (const string of TEXT_NUMBERS) {
        const check = stringToNumber(testString, string);

        if(check !== null) {
            foundNumber = check;
            break;
        }
    }

   if(foundNumber === null) {
         foundNumber = parseInt(testString) ?? null;
   }

    return foundNumber;
};

const getFirstDigit = (fullNumber: number): number => {
    return parseInt(fullNumber.toString().charAt(0));
};

const getLastDigit = (fullNumber: number): number => {
    return parseInt(fullNumber.toString().charAt(fullNumber.toString().length - 1));
};

const getLineNumber = (first: number, second: number): number => {
    return parseInt(first.toString() + second.toString());
};

let counter = 0;

for (const calibrationLine of calibrationLines) {
    const numbers = findNumbers(calibrationLine);
    const firstDigit = getFirstDigit(numbers[0]);
    const lastDigit = getLastDigit(numbers[numbers.length - 1]);
    const lineNumber = getLineNumber(firstDigit, lastDigit);

    console.log(calibrationLine, ": ", lineNumber, " - ", firstDigit, " - ", lastDigit, " | ",  numbers);
    counter += lineNumber;
}

console.log("count: ", counter);
