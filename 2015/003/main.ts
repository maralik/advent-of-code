import {INPUT} from "./input";

const UP = '^';
const DOWN = 'v';
const LEFT = '<';
const RIGHT = '>';

const test = INPUT;

type Position = {
    x: number,
    y: number,
}

const DIMENSIONS: {
    [key: string]: number
} = {};

let currentPositionSanta: Position = {x: 0, y: 0};

const keyGenerator = (position: Position): string => {
    return `${position.x}_${position.y}`;
}

const logPositionVisit = (position: Position): void => {
    let key = keyGenerator(position);

    if (DIMENSIONS[key]) {
        DIMENSIONS[key] += 1;
    } else {
        DIMENSIONS[key] = 1;
    }
}

const movePosition = (direction: string, position: Position): void => {
    switch (direction) {
        case UP:
            position.y += 1;
            break;
        case DOWN:
            position.y -= 1;
            break;
        case LEFT:
            position.x -= 1;
            break;
        case RIGHT:
            position.x += 1;
            break;
    }
}

logPositionVisit(currentPositionSanta);

for (const direction of test) {
    movePosition(direction, currentPositionSanta);
    logPositionVisit(currentPositionSanta);
}


let moreThanOnce = 0;
for (const key of Object.keys(DIMENSIONS)) {
    if (DIMENSIONS[key] > 1) {
        moreThanOnce++;
    }
}

console.log("At least once:", Object.keys(DIMENSIONS).length);
console.log("More then once:", moreThanOnce);
