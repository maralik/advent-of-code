import {INPUT} from "./input";

type Direction = '^' | 'v' | '<' | '>';

const UP: Direction = '^';
const DOWN: Direction = 'v';
const LEFT: Direction = '<';
const RIGHT: Direction = '>';

const Directions: Direction[] = [UP, DOWN, LEFT, RIGHT] as const;

type Position = {
    x: number,
    y: number,
}

class Courieer {
    protected position: Position = {
        x: 0,
        y: 0
    };

    public constructor(initialPosition?: Position) {
        if(initialPosition) {
            this.position = initialPosition;
        }
    }

    public move(direction: Direction): Position {
        switch (direction) {
            case UP:
                this.position.y++;
                break;
            case DOWN:
                this.position.y--;
                break;
            case LEFT:
                this.position.x++;
                break;
            case RIGHT:
                this.position.x--;
                break;
            default:
                throw new Error('Unknown direction');
                break;
        }

        return this.position;
    }
}

class DeliveryPlan {
    protected plan: Direction[];
    protected currentDelivery = 0;
    public constructor(map: string) {
        this.plan = this.parseMap(map);
    }

    public get currentDeliveryIndex(): number {
        return this.currentDelivery;
    }

    public nextDirection(): Direction | undefined {
        const currentDelivery = this.plan[this.currentDelivery];

        if(currentDelivery) {
            this.currentDelivery++;
        }

        return currentDelivery;

    }

    protected parseMap(map: string): Direction[] {
        return map.split('').map((direction, index) => {
            if(!Directions.includes(direction as Direction)) {
                throw new Error(`Invalid direction ${direction} on position ${index}`);
            }

            return direction as Direction;
        });
    }
}

class DeliveryManager {
    protected plan: DeliveryPlan;
    protected couriers: Courieer[];
    protected visitedHouses: { [key: string]: number} = {};
    public constructor(plan: DeliveryPlan, couriers: Courieer[]) {
        this.plan = plan;
        this.couriers = couriers;
    }

    public deliverPackages(): void {
        let currentDirection: Direction | undefined = this.plan.nextDirection();

        while(currentDirection) {
            const position = this.currentCourier().move(currentDirection);
            this.logDeliveryPosition(position);

            currentDirection = this.plan.nextDirection();
        }
    }

    public countUniqueHouses(): number {
        return Object.keys(this.visitedHouses).length;
    }

    protected currentCourier(): Courieer {
        return this.couriers[0];
    }

    protected logDeliveryPosition(position: Position): void {
      const deliveryPosition = `${position.x}_${position.y}`;

      if(this.visitedHouses[deliveryPosition]) {
          this.visitedHouses[deliveryPosition]++;
      } else {
          this.visitedHouses[deliveryPosition] = 1;
      }
    }
}

class DeliveryManagerWithRobot extends DeliveryManager {
    protected currentCourier(): Courieer {
        if (this.couriers.length !== 2 ) {
            throw Error('Invalid count of Couriers')
        }

        return this.plan.currentDeliveryIndex % 2 ? this.couriers[0] : this.couriers[1];
    }
}

const plan = new DeliveryPlan(INPUT);
const santaOnly = new Courieer();
const manager = new DeliveryManager(plan, [santaOnly]);

manager.deliverPackages();
console.log('Santa alone:', manager.countUniqueHouses());


const planWithRobot = new DeliveryPlan(INPUT);
const santa = new Courieer();
const robot = new Courieer();
const manager2 = new DeliveryManagerWithRobot(plan, [santa, robot]);

manager2.deliverPackages();
console.log('Santa with robot:', manager.countUniqueHouses());
