import {createHash} from "crypto";

const INPUT = 'abcdef';
const md5hash = (data: string): string => {
    return createHash('md5').update(data).digest('hex');
};
const isStartsWithFiveZeros = (data: string) => data.slice(0, 5) === '00000';

let counter = 0;

while (true) {
    const hash = md5hash(INPUT + counter.toString())
    const match = isStartsWithFiveZeros(hash);

    console.log(INPUT + counter.toString())
    if (match) {
        console.log(hash);
        break;
    }

    counter++
};

console.log(counter);

