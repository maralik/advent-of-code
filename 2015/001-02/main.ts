import {INPUT} from "./input";

const instructions = INPUT;

const instructionsArray = instructions.split("");


let floor = 0;

instructionsArray.forEach((instruction: string, key: number) => {
    if(instruction === "(") {
        floor++;
    } else {
        floor--;
    }

    if(floor === -1) {
       console.log("basement ", key + 1);
    }
});

console.log(floor);
