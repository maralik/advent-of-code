"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const input_1 = require("./input");
const lines = input_1.INPUT.split("\n");
class Package {
    constructor(dimensions) {
        const parsed = this.dimensionParser(dimensions);
        this.l = parsed.l;
        this.h = parsed.h;
        this.w = parsed.w;
    }
    ribbonNeeded() {
        const shortestSides = [this.l, this.w, this.h].sort((a, b) => a - b);
        return 2 * shortestSides[0] + 2 * shortestSides[1] + this.l * this.w * this.h;
    }
    paperNeeded() {
        const firstSide = this.l * this.w;
        const secondSide = this.w * this.h;
        const thirdSide = this.h * this.l;
        const shortestSide = [firstSide, secondSide, thirdSide].sort((a, b) => a - b);
        return 2 * firstSide + 2 * secondSide + 2 * thirdSide + shortestSide[0];
    }
    dimensionParser(dimensions) {
        const arr = dimensions.split('x');
        return {
            l: parseInt(arr[0]),
            h: parseInt(arr[1]),
            w: parseInt(arr[2])
        };
    }
}
let paper = 0;
let ribbon = 0;
lines.forEach((line, key) => {
    const pack = new Package(line);
    paper += pack.paperNeeded();
    ribbon += pack.ribbonNeeded();
});
// console.log(paper);
console.log(ribbon);
