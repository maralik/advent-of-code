import {INPUT} from "./input";

const lines = INPUT.split("\n");

interface Dimensions {
    l: number;
    h: number;
    w: number;
}

class Package {
    private readonly l: number;
    private readonly h: number;
    private readonly w: number;

    public constructor(dimensions: string) {
        const parsed = this.dimensionParser(dimensions);

        this.l = parsed.l;
        this.h = parsed.h;
        this.w = parsed.w;
    }

    public ribbonNeeded(): number {
        const shortestSides = [this.l, this.w, this.h].sort((a,b,) => a-b);

        return 2 * shortestSides[0] + 2 * shortestSides[1] + this.l * this.w * this.h;
    }

    public paperNeeded(): number {
        const firstSide = this.l * this.w;
        const secondSide = this.w * this.h;
        const thirdSide = this.h * this.l;

        const shortestSide = [firstSide, secondSide, thirdSide].sort((a,b) => a-b);

        return 2 * firstSide + 2 * secondSide + 2 * thirdSide + shortestSide[0];
    }

    protected dimensionParser(dimensions: string): Dimensions {
        const arr = dimensions.split('x');

        return {
            l: parseInt(arr[0]),
            h: parseInt(arr[1]),
            w: parseInt(arr[2])
        }
    }
}

let paper = 0;
let ribbon = 0;
lines.forEach((line: string, key: number) => {

    const pack = new Package(line);

    paper += pack.paperNeeded();
    ribbon += pack.ribbonNeeded();

});
console.log("Paper", paper);
console.log("Ribbon", ribbon);
