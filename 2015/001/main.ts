import {INPUT} from "./input";

const instructions = INPUT;

const instructionsArray = instructions.split("");


let floor = 0;

instructionsArray.forEach((instruction: string) => {
    if(instruction === "(") {
        floor++;
    } else {
        floor--;
    }
});

// console.log(floor);


let input = ')';
console.log(input.split('('));
input = '(';
console.log(input.split('('));
input = '()()';
console.log(input.split('('));
input = '()()((()';
console.log(input.split('('));
