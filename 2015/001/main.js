"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const input_1 = require("./input");
const instructions = input_1.INPUT;
const instructionsArray = instructions.split("");
let floor = 0;
instructionsArray.forEach((instruction) => {
    if (instruction === "(") {
        floor++;
    }
    else {
        floor--;
    }
});
console.log(floor);
